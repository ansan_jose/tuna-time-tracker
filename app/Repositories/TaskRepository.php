<?php
namespace App\Repositories;
use App\Task;
use Illuminate\Support\Collection;
/**
 * TaskRepository Repository
 * @package App\Repositories
 */
class TaskRepository extends Repository
{
    public function all()
    {
        return Task::all();
    }

    public function findById($id)
    {
        return Task::find($id);
    }

    public function create(){
        return new Task();
    }

    public function save($all){
        return Task::create($all);   
    }

    public function update($id,$all){
        return Task::find($id)->fill($all)->save();
    }

    public function destroy($id){
        return Task::find($id)->delete();
    }
}