<?php
namespace App\Repositories;
use App\TimeTracker;
use Illuminate\Support\Collection;
/**
 * TaskRepository Repository
 * @package App\Repositories
 */
class TimeTrackerRepository extends Repository
{
    public function all()
    {
        return TimeTracker::all();
    } 

    public function findById($id)
    {
        return TimeTracker::find($id);
    }

    public function create(){
        return new TimeTracker();
    }

    public function save($all){
        return TimeTracker::create($all);   
    }

    public function update($id,$all){
        return TimeTracker::find($id)->fill($all)->save();
    }

    public function destroy($id){
        return TimeTracker::find($id)->delete();
    }     
    public function timeTrack($id){
        return TimeTracker::where('user_id',$id)
        ->select('task_id',\DB::raw('SUM(time) as total_time'))
        ->groupBy('task_id')
        ->get();
    }  
}