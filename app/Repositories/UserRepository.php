<?php
namespace App\Repositories;
use App\User;
use Illuminate\Support\Collection;
/**
 * UserRepository Repository
 * @package App\Repositories
 */
class UserRepository extends Repository
{
    public function all()
    {
        return User::all();
    }

    public function findById($id)
    {
        return User::find($id);
    }

    public function create(){
        return new User;
    }

    public function save($all){
        return User::create($all);
    }

    public function update($id,$all){
        return User::find($id)->fill($all)->save();
    }
    public function destroy($id){
        return User::find($id)->delete();
    }  
}