<?php

namespace App;


use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TimeTracker extends Model
{
    use Notifiable;
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'task_id','user_id','time'
    ];
   
    protected function user(){
        return $this->belongsTo(User::class,'user_id');
    }

    protected function task(){
        return $this->belongsTo(Task::class,'task_id');
    }
}
