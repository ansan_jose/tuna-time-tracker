<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Repositories\UserRepository;
use App\Repositories\TimeTrackerRepository;

class UserController extends Controller
{

    protected $userRepo;

    public function __construct()
    {
        $this->middleware('auth');
        $this->userRepo = new UserRepository();
        $this->timeTrackerRepo = new TimeTrackerRepository();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $users = $this->userRepo->all();

        return view('user.index', [
            'data' => $users,
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data       = $this->userRepo->create();

        return view('user.edit', [
            'data' => $data,
        ]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $all = $request->all();    

        $validated = $request->validate([
            'name' => 'required',
            'email' => 'required|unique:users',
            'password' => 'required|confirmed|min:6',
            'password_confirmation' => 'required|min:6',
        ]);    

        $all['password'] = bcrypt($all['password']);

        if($validated)
            $save = $this->userRepo->save($all);
       
        return redirect("user");

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = $this->userRepo->findById($id);
        
        if(empty($data))
            return redirect('user');

        return view('user.edit', [
            'data' => $data,
        ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $all = $request->all();

        $validated = $request->validate([
            'name' => 'required',
            'email' => 'required|unique:users,email,'.$id,
            'password' => 'confirmed',
        ]);     

        if($all['password'])
            $all['password'] = bcrypt($all['password']);
        else
            unset($all['password']);

        if($validated)
            $this->userRepo->update($id,$all);

        return redirect("user");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $this->userRepo->destroy($request->id);

        return redirect("user");

    }

    /**
     * display time tracker details of user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $timeTracks = $this->timeTrackerRepo->timeTrack($id);

        return view('user.time-tracks', [
            'data' => $timeTracks,
        ]);
    }
}
