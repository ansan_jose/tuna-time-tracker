<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Repositories\TaskRepository;
use App\Repositories\UserRepository;
use App\Repositories\TimeTrackerRepository;

class TimeTrackerController extends Controller
{

    protected $taskRepo;
    protected $userRepo;

    public function __construct()
    {
        $this->middleware('auth');
        $this->taskRepo = new TaskRepository();
        $this->userRepo = new UserRepository();
        $this->timeTrackerRepo = new TimeTrackerRepository();

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $data = $this->timeTrackerRepo->all();

        return view('time-tracker.index', [
            'data' => $data,
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data       = $this->timeTrackerRepo->create();
        $users       = $this->userRepo->all();
        $tasks       = $this->taskRepo->all();

        return view('time-tracker.edit', [
            'data' => $data,
            'users' => $users,
            'tasks' => $tasks
        ]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $all = $request->all();    

        $validated = $request->validate([
            'task_id' => 'required|numeric',
            'user_id' => 'required|numeric',
            'time' => 'required',
        ]);    

        if($validated)
            $save = $this->timeTrackerRepo->save($all);
       
        return redirect("time-tracker");

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = $this->timeTrackerRepo->findById($id);
        $users       = $this->userRepo->all();
        $tasks       = $this->taskRepo->all();
        
        if(empty($data))
            return redirect('time-tracker');

        return view('time-tracker.edit', [
            'data' => $data,
            'users' => $users,
            'tasks' => $tasks
        ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $all = $request->all();    
        $old_task= $this->taskRepo->findById($id);

        $validated = $request->validate([
            'task_id' => 'required|numeric',
            'user_id' => 'required|numeric',
            'time' => 'required',
        ]);    

        if($validated)
            $save = $this->timeTrackerRepo->update($id,$all);
       
        return redirect("time-tracker");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $this->timeTrackerRepo->destroy($request->id);

        return redirect("time-tracker");

    }
}
