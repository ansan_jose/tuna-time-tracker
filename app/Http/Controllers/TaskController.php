<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Repositories\TaskRepository;
use App\Repositories\UserRepository;

class TaskController extends Controller
{

    protected $taskRepo;
    protected $userRepo;

    public function __construct()
    {
        $this->middleware('auth');
        $this->taskRepo = new TaskRepository();
        $this->userRepo = new UserRepository();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $tasks = $this->taskRepo->all();

        return view('task.index', [
            'data' => $tasks,
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data       = $this->taskRepo->create();
        $users       = $this->userRepo->all();

        return view('task.edit', [
            'data' => $data,
            'users' => $users
        ]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $all = $request->all();    

        $validated = $request->validate([
            'task' => 'required',
            'user_id' => 'required|numeric',
        ]);    

        if($validated)
            $save = $this->taskRepo->save($all);
       
        return redirect("task");

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = $this->taskRepo->findById($id);
        $users       = $this->userRepo->all();
        
        if(empty($data))
            return redirect('task');

        return view('task.edit', [
            'data' => $data,
            'users' => $users
        ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $all = $request->all();

        $validated = $request->validate([
            'task' => 'required',
            'user_id' => 'required|numeric',
        ]);    

        if($validated)
            $this->taskRepo->update($id,$all);

        return redirect("task");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $this->taskRepo->destroy($request->id);

        return redirect("task");

    }
}
