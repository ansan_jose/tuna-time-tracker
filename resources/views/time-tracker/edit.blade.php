@extends('layouts.app')

@section('content')

	<div class="container">        
        <div class="card">
            <div class="card-header">
                @if($data->id==0) Add New Time Tracker @else Edit Time Tracker @endif
            </div>
            
            <form  method="POST" action="{{ url('time-tracker' . ($data->id ? "/$data->id" : '')) }}">
                <div class="card-body">
                    @if($data->id)
                        {{ method_field('PUT') }}
                    @endif

                    {{ csrf_field() }}

                    <div class="form-group row ">
                        <label for="task" class="col-sm-1 col-form-label">Task </label>
                        <div class="input-group col-sm-6">
                            <select class="form-control {{ $errors->has('task_id') ? ' is-invalid' : '' }}" name="task_id">
                                <option value="" > Select </option>
                                @foreach ($tasks as $task)
                                    @if($task->id == old('task_id') || $task->id == $data->task_id))
                                    <option value="{{$task->id}}" selected="selected">{{$task->task}}</option>
                                    @else
                                        <option value="{{$task->id}}" >{{$task->task}}</option>
                                    @endif
                                @endforeach
                            </select>

                            @if ($errors->has('task_id'))
                                <span role="alert" class="invalid-feedback"><strong>{{ $errors->first('task_id') }}</strong></span>
                            @endif
                            
                        </div>
                    </div>           

                    <div class="form-group row">
                        <label for="user_id" class="col-sm-1 col-form-label">User</label>
                        <div class="input-group col-sm-4">
                            <select class="form-control {{ $errors->has('user_id') ? ' is-invalid' : '' }}" name="user_id">
                                <option value="" > Select </option>
                                @foreach ($users as $user)
                                    @if($user->id == old('user_id') || $user->id == $data->user_id))
                                    <option value="{{$user->id}}" selected="selected">{{$user->name}}</option>
                                    @else
                                        <option value="{{$user->id}}" >{{$user->name}}</option>
                                    @endif
                                @endforeach
                            </select>

                            @if ($errors->has('user_id'))
                                <span role="alert" class="invalid-feedback"><strong>{{ $errors->first('user_id') }}</strong></span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="user_id" class="col-sm-1 col-form-label">Time</label>
                        <div class="input-group col-sm-4">
                           <input type="text"  id="start_time" class="form-control col-md-6 mr-2" name="start_time"  value="{{old('start_time')}}">
                           <input type="text"  id="end_time" class="form-control col-md-6 mr-2" name="end_time" value="{{old('end_time')}}">
                           <input type="text" readonly id="time" class="form-control col-md-6 {{ $errors->has('time') ? ' is-invalid' : '' }}" value="{{gmdate('H:i:s',old('time',$data->time))}}">
                           <input type="hidden" id="hid_time" name="time" value="{{old('time',$data->time)}}">

                            @if ($errors->has('time'))
                                <span role="alert" class="invalid-feedback"><strong>{{ $errors->first('time') }}</strong></span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="user_id" class="col-sm-1 col-form-label"> &nbsp; </label>   
                        <div class="col-sm-4">
                            <input type="button" id="timer-btn" class="btn btn-success" value="Start Timer">
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="col-md-12">    
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <a href="{{url('time-tracker')}}" class="btn btn-secondary">Cancel</a>
                    </div>
                </div>

                </form>
            </div>
        </div>
    </div>
@endsection

@section('footer')
    <script>
        $(document).ready(function() {
            $("#timer-btn").on('click',function(){
                if($(this).val() == "Start Timer"){
                    var currentTime = new Date();
                    var timeText = currentTime.getHours()+":"+currentTime.getMinutes()+":"+currentTime.getSeconds();
                    $("#start_time").val(timeText);
                    $("#end_time").val('');
                    $("#time").val('');
                    $("#hid_time").val('');

                    $(this).val("Stop Timer").addClass("btn-danger").removeClass("btn-success");
                }
                else{
                    var currentTime = new Date();
                    var timeText = currentTime.getHours()+":"+currentTime.getMinutes()+":"+currentTime.getSeconds();
                    $("#end_time").val(timeText);

                    var diff = (new Date("2021-02-19 " + $("#end_time").val()) - new Date("2021-02-19 " +$("#start_time").val()));

                    $("#time").val(new Date(diff).toISOString().substr(11, 8));
                    $("#hid_time").val(diff/1000);

                    $(this).val("Start Timer").addClass("btn-success").removeClass("btn-danger");
                }
            });
        });
    </script>
@endsection