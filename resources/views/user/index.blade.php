@extends('layouts.app')
<style>
    th{
        background: #f5f5f5;
        padding:10px;
    }
    td{
        padding:10px;
        border-bottom: 1px solid #ccc;
    }
    a input{
        background: none;
        border:none;
        cursor: pointer;
        color:#3490dc;
    }
</style>
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12" >
                <div class="card">
                    <div class="card-header">User                   
                        <a href="{{url('user/create')}}" class="btn btn-success btn-sm float-right">
                            Add User
                        </a>                   
                    </div>
                    <div class="card-body" style="height:500px;">
                        <table cellspacing="0" width="100%" >
                            <thead>
                                <tr>
                                    <th width="300" >Name</th>
                                    <th width="300">Email</th>
                                    <th width="60" ></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach( $data as $user)
                                <tr  >
                                    <td >{{$user->name}}</td>
                                    <td >{{$user->email}}</td>
                                    <td > <a href="{{url('user/'.$user->id)}}" > Time Track </a> | 
                                        <a href="{{url('user/'.$user->id.'/edit')}}" > Edit </a> | 
                                        <a click="#" > 
                                        <form action="{{url('user/delete')}}" method="POST" style="display:inherit;">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                            <input type="hidden" name="id" value="{{$user->id}}">
                                            <input type="submit" value="Delete"> 
                                        </form> </a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection