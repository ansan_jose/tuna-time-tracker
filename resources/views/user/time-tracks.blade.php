@extends('layouts.app')
<style>
    th{
        background: #f5f5f5;
        padding:10px;
    }
    td{
        padding:10px;
        border-bottom: 1px solid #ccc;
    }
    a input{
        background: none;
        border:none;
        cursor: pointer;
        color:#3490dc;
    }
</style>
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12" >
                <div class="card">
                    <div class="card-header">User Time Track   
                        <a href="{{url('user')}}" class="btn btn-secondary  btn-sm float-right">
                            Back
                        </a> 
                    </div>
                    <div class="card-body" style="height:500px;">
                        <table cellspacing="0" width="100%" >
                            <thead>
                                <tr>
                                    <th width="300" >Task</th>
                                    <th width="300">Total Time</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach( $data as $user)
                                <tr  >
                                    <td >{{$user->task->task}}</td>
                                    <td >{{gmdate("H:i:s",$user->total_time)}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection