@extends('layouts.app')

@section('content')

	<div class="container">        
        <div class="card">
            <div class="card-header">
                @if($data->id==0) Add New User @else Edit User @endif
            </div>
            
            <form  method="POST" action="{{ url('user' . ($data->id ? "/$data->id" : '')) }}">
                <div class="card-body">
                    @if($data->id)
                        {{ method_field('PUT') }}
                    @endif

                    {{ csrf_field() }}

                    <div class="form-group row ">
                        <label for="name" class="col-sm-2 col-form-label">Task </label>
                        <div class="input-group col-sm-6">
                            <input type="text" name="name" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" maxlength="255" placeholder="Task" value="{{ old('name',$data->name)}}">

                            @if ($errors->has('name'))
                                <span role="alert" class="invalid-feedback"><strong>{{ $errors->first('name') }}</strong></span>
                            @endif
                            
                        </div>
                    </div>    

                    <div class="form-group row ">
                        <label for="email" class="col-sm-2 col-form-label">Email </label>
                        <div class="input-group col-sm-6">
                            <input type="email" name="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" maxlength="255" placeholder="Email" value="{{ old('email',$data->email)}}">

                            @if ($errors->has('email'))
                                <span role="alert" class="invalid-feedback"><strong>{{ $errors->first('email') }}</strong></span>
                            @endif
                            
                        </div>
                    </div>           

                    <div class="form-group row ">
                        <label for="password" class="col-sm-2 col-form-label">Password </label>
                        <div class="input-group col-sm-6">
                            <input type="password" name="password" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" maxlength="255" placeholder="Password" value="{{ old('password')}}">

                            @if ($errors->has('password'))
                                <span role="alert" class="invalid-feedback"><strong>{{ $errors->first('password') }}</strong></span>
                            @endif
                            
                        </div>
                    </div>    

                    <div class="form-group row ">
                        <label for="password_confirmation" class="col-sm-2 col-form-label">Confirm Password </label>
                        <div class="input-group col-sm-6">
                            <input type="password" name="password_confirmation" class="form-control {{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}" maxlength="255" placeholder="Confirm Password" value="{{ old('password_confirmation')}}">

                            @if ($errors->has('password_confirmation'))
                                <span role="alert" class="invalid-feedback"><strong>{{ $errors->first('password_confirmation') }}</strong></span>
                            @endif
                            
                        </div>
                    </div>        
                    
                </div>
                <div class="card-footer">
                    <div class="col-md-12">                    
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <a href="{{url('user')}}" class="btn btn-secondary">Cancel</a>
                    </div>
                </div>

                </form>
            </div>
        </div>
    </div>
@endsection