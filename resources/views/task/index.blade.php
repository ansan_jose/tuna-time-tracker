@extends('layouts.app')
<style>
    th{
        background: #f5f5f5;
        padding:10px;
    }
    td{
        padding:10px;
        border-bottom: 1px solid #ccc;
    }
    a input{
        background: none;
        border:none;
        cursor: pointer;
        color:#3490dc;
    }
</style>
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12" >
                <div class="card">
                    <div class="card-header">Tasks                   
                        <a href="{{url('task/create')}}" class="btn btn-success btn-sm float-right">
                            Add Task
                        </a>                   
                    </div>
                    <div class="card-body" style="height:500px;">
                        <table cellspacing="0" width="100%" >
                            <thead>
                                <tr>
                                    <th width="300" >Task</th>
                                    <th width="300">User</th>
                                    <th width="60" ></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach( $data as $task)
                                <tr  >
                                    <td >{{$task->task}}</td>
                                    <td >{{$task->user->name}}</td>
                                    <td > <a href="{{url('task/'.$task->id.'/edit')}}" > Edit </a> | 
                                        <a click="#" > 
                                        <form action="{{url('task/delete')}}" method="POST" style="display:inherit;">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                            <input type="hidden" name="id" value="{{$task->id}}">
                                            <input type="submit" value="Delete"> 
                                        </form> </a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection