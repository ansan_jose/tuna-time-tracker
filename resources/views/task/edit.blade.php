@extends('layouts.app')

@section('content')

	<div class="container">        
        <div class="card">
            <div class="card-header">
                @if($data->id==0) Add New Task @else Edit Task @endif
            </div>
            
            <form  method="POST" action="{{ url('task' . ($data->id ? "/$data->id" : '')) }}">
                <div class="card-body">
                    @if($data->id)
                        {{ method_field('PUT') }}
                    @endif

                    {{ csrf_field() }}

                    <div class="form-group row ">
                        <label for="task" class="col-sm-1 col-form-label">Task </label>
                        <div class="input-group col-sm-6">
                            <input type="text" name="task" class="form-control {{ $errors->has('task') ? ' is-invalid' : '' }}" maxlength="255" placeholder="Task" value="{{ old('task',$data->task)}}">

                            @if ($errors->has('task'))
                                <span role="alert" class="invalid-feedback"><strong>{{ $errors->first('task') }}</strong></span>
                            @endif
                            
                        </div>
                    </div>           

                    <div class="form-group row">
                        <label for="user_id" class="col-sm-1 col-form-label">User</label>
                        <div class="input-group col-sm-4">
                            <select class="form-control {{ $errors->has('user_id') ? ' is-invalid' : '' }}" name="user_id">
                                <option value="" > Select </option>
                                @foreach ($users as $user)
                                    @if($user->id == old('user_id') || $user->id == $data->user_id))
                                    <option value="{{$user->id}}" selected="selected">{{$user->name}}</option>
                                    @else
                                        <option value="{{$user->id}}" >{{$user->name}}</option>
                                    @endif
                                @endforeach
                            </select>

                            @if ($errors->has('user_id'))
                                <span role="alert" class="invalid-feedback"><strong>{{ $errors->first('user_id') }}</strong></span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="col-md-12">                    
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <a href="{{url('task')}}" class="btn btn-secondary">Cancel</a>
                    </div>
                </div>

                </form>
            </div>
        </div>
    </div>
@endsection